import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { SearchBox, WeatherForecastSection } from "./components";
import {
  getCityForecast,
  ICityInformation,
  searchCityByKeyword,
  IConsolidatedWeather,
} from "./services/weatherApi";

function App() {
  const [keyword, setKeyword] = useState<string>("");
  const [selectedCity, setSelectedCity] = useState<ICityInformation | null>(
    null
  );
  const [cities, setCities] = useState<ICityInformation[]>([]);
  const [cityForecastInfo, setCityForecastInfo] = useState<
    IConsolidatedWeather[]
  >([]);
  const [isLoading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (keyword && (!selectedCity || keyword !== selectedCity.title))
      searchCityByKeyword(keyword).then((data) => setCities(data));
  }, [keyword, selectedCity]);

  useEffect(() => {
    setLoading(true);
    if (selectedCity)
      getCityForecast(selectedCity.woeid).then((data) => {
        setLoading(false);
        setCityForecastInfo(data.consolidated_weather);
      });
  }, [selectedCity]);

  const handleSelectedCityChanged = (city: ICityInformation) => {
    setSelectedCity(city);
    setKeyword(city.title);
  };

  const handleKeywordChanged = (newKey: string) => {
    setKeyword(newKey);
  };

  return (
    <Container>
      <Row>
        <Col xs={12} md={4}>
          <SearchBox
            keyword={keyword}
            onKeywordChanged={handleKeywordChanged}
            cities={cities}
            onSelectedCityChanged={handleSelectedCityChanged}
          />
        </Col>
      </Row>
      <Row style={{ justifyContent: "center" }}>
        <WeatherForecastSection
          isLoading={isLoading}
          cityForecastInfo={cityForecastInfo}
        />
      </Row>
    </Container>
  );
}

export default App;

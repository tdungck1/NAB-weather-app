export interface ICityInformation {
location_type: string
title: string
woeid: number
}

export interface IConsolidatedWeather {
    id: number,
    weather_state_name: string,
    weather_state_abbr: string,
    applicable_date: string,
    min_temp: number,
    max_temp: number,
}

export interface ICityForecast {
    consolidated_weather: IConsolidatedWeather[]
}

export const searchCityByKeyword = (key: string):Promise<ICityInformation[]> => {
    return fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/search/?query=${key}`,
    {headers: {"X-Requested-With": "XMLHttpRequest"}})
    .then(response => {
        if (!response.ok) throw Error(response.statusText);
        return response.json();
      })
      .then(data => {
        return data
      })
      .catch(error => {
        console.log("error", error);
      });
}

export const getCityForecast = (cityCode: number):Promise<ICityForecast> => {
    return fetch(`https://cors-anywhere.herokuapp.com/https://www.metaweather.com/api/location/${cityCode}/`,
    {headers: {"X-Requested-With": "XMLHttpRequest"}})
        .then(response => {
            if (!response.ok)
                throw Error(response.statusText);
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(error => {
            console.log("error", error);
        });
}
import React from "react";
import { Col } from "react-bootstrap";
import { IConsolidatedWeather } from "../../services/weatherApi";

interface IWeatherForecastSectionItemProps {
  forecastData: IConsolidatedWeather;
}

export const WeatherForecastSectionItem: React.FC<IWeatherForecastSectionItemProps> = ({
  forecastData,
}) => {
  const { applicable_date, min_temp, max_temp } = forecastData;
  return (
    <Col className="m-2 border">
      <div className="mb-2">{getDayOfWeek(applicable_date)}</div>
      <div>Min: {Math.round(min_temp)}</div>
      <div>Max: {Math.round(max_temp)}</div>
    </Col>
  );
};

const getDayOfWeek = (value: string) => {
  let weekday = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  return weekday[new Date(value).getDay()];
};

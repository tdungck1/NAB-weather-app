import React from "react";
import { Spinner } from "react-bootstrap";
import { IConsolidatedWeather } from "../../services/weatherApi";
import { WeatherForecastSectionItem } from "./WeatherForecastSectionItem";

export interface IWeatherForecastSectionProps {
  cityForecastInfo: IConsolidatedWeather[];
  isLoading: boolean;
}
export const WeatherForecastSection: React.FC<IWeatherForecastSectionProps> = ({
  cityForecastInfo,
  isLoading,
}) => {
  return (
    <>
      {isLoading ? (
        <Spinner animation="border" role="status">
          <span className="sr-only">Loading...</span>
        </Spinner>
      ) : (
        cityForecastInfo
          .slice(0, 5)
          .map((forecastData: IConsolidatedWeather) => (
            <WeatherForecastSectionItem
              forecastData={forecastData}
              key={forecastData.id}
            />
          ))
      )}
    </>
  );
};

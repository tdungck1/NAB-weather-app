import React from "react";
import Enzyme, { shallow } from "enzyme";
import { WeatherForecastSection, IWeatherForecastSectionProps } from "./index";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const mockWeatherForecastSectionProps: IWeatherForecastSectionProps = {
  cityForecastInfo: [
    {
      id: 5164477443997696,
      weather_state_name: "Light Rain",
      weather_state_abbr: "lr",
      applicable_date: "2017-06-05",
      min_temp: 12.515714285714285,
      max_temp: 17.098571428571429,
    },
  ],
  isLoading: false,
};

describe("WeatherForecastSection component", () => {
  it("renders without exploding", () => {
    const wrapper = shallow(
      <WeatherForecastSection {...mockWeatherForecastSectionProps} />
    );
    expect(wrapper.exists()).toBe(true);
  });
});

import React from "react";
import Enzyme, { shallow } from "enzyme";
import { SearchBox } from "./index";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

const mockSearchBoxProps = {
  keyword: "test keyword",
  onKeywordChanged: () => {
    ("");
  },
  cities: [],
  onSelectedCityChanged: () => {
    ("");
  },
};

describe("SearchBox component", () => {
  it("renders without exploding", () => {
    const wrapper = shallow(<SearchBox {...mockSearchBoxProps} />);
    expect(wrapper.exists()).toBe(true);
  });
});

import React, { useState } from "react";
import { Form, InputGroup, ListGroup } from "react-bootstrap";
import { ICityInformation } from "../../services/weatherApi";

interface ISearchBoxProps {
  keyword: string;
  onKeywordChanged: (key: string) => void;
  cities: ICityInformation[];
  onSelectedCityChanged: (city: ICityInformation) => void;
}

export const SearchBox: React.FC<ISearchBoxProps> = ({
  keyword,
  onKeywordChanged,
  cities,
  onSelectedCityChanged,
}) => {
  const [isInputFocused, setInputFocused] = useState<boolean>(false);

  const handleChange = (e: any) => {
    onKeywordChanged(e.target.value);
  };

  const handleClick = (e: any) => {
    setInputFocused(false);
    const cityName: string = e.target.innerText;
    cities.forEach((city) => {
      if (city.title === cityName) {
        onSelectedCityChanged(city);
      }
    });
  };

  return (
    <>
      <InputGroup className="mb-3 mt-4">
        <InputGroup.Prepend>
          <InputGroup.Text>
            <i className="fa fa-search"></i>
          </InputGroup.Text>
        </InputGroup.Prepend>
        <Form.Control
          type="text"
          value={keyword}
          placeholder="Search"
          onChange={handleChange}
          onFocus={() => {
            setInputFocused(true);
          }}
        />
      </InputGroup>
      <ListGroup
        style={{
          zIndex: 999,
          position: "absolute",
          right: "15px",
          left: "15px",
        }}
        hidden={!isInputFocused}
      >
        {cities && cities.length > 0
          ? cities.map((city) => (
              <ListGroup.Item onClick={handleClick} key={city.woeid}>
                {city.title}
              </ListGroup.Item>
            ))
          : null}
      </ListGroup>
    </>
  );
};
